from django.core.exceptions import ValidationError
from django.utils import timezone
from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label="Username", max_length=20)
	password = forms.CharField(label="Password", max_length=20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label="Status", max_length=50)

def validate_event_date(value):
	if value < timezone.now().date():
		raise ValidationError("Event date can not be in the past")

class EventForm(forms.Form):
	event_name = forms.CharField(label="Event Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	event_date = forms.DateField(label="Event Date", validators=[validate_event_date])

class UpdateProfileForm(forms.Form):
	first_name = forms.CharField(label="First Name", max_length=50)
	last_name = forms.CharField(label="Last Name", max_length=50)
	password = forms.CharField(label="Password", max_length=50, required=False)
	new_password = forms.CharField(label="New Password", max_length=50, required=False)

class RegisterForm(forms.Form):
	first_name = forms.CharField(label="First Name", max_length=50)
	last_name = forms.CharField(label="Last Name", max_length=50)
	email = forms.CharField(label="Email", max_length=50)
	username = forms.CharField(label="Username", max_length=50)
	password = forms.CharField(label="Password", max_length=50)
