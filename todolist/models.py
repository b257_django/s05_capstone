from django.db import models
from django.contrib.auth.models import User
import datetime


class ToDoItem(models.Model):
	task_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "Pending")
	date_created = models.DateTimeField("Date Created")
	# Adding a user_id to the ToDoItem table, which is a foreign key connected to the Users table.
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")

class MyEvents(models.Model):
	event_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default="Pending")
	event_date = models.DateField(default=datetime.date.today)
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")